package fibonacci;

import java.util.ArrayList;
import java.util.List;

/**
 *The {@code fibonacci.FibonacciHandler} class for generating Fibonacci numbers.
 *
 * @author Oleh Ferents
 */
public class FibonacciHandler {
    /**
     * Field that contains value of first number of Fibonacci numbers.
     */
    private int f1;

    /**
     * Field that contains value of second number of Fibonacci numbers.
     */
    private int f2;

    /**
     * Field for representing one hundred percent.
     */
    private static final int HUNDRED = 100;

    /**
     * List that contains Fibonacci numbers.
     */
    private List<Integer> fibonacciNums = new ArrayList<>();

    /**
     * Method for generating list of Fibonacci numbers.
     * @param n - size of list
     * @param biggestOddNum - the biggest odd number
     * @param biggestEvenNum - the biggest even number
     */
    public void buildSet(final int n,
                         final int biggestOddNum,
                         final int biggestEvenNum) {
        f1 = biggestOddNum;
        f2 = biggestEvenNum;

        fibonacciNums.clear();

        for (int i = 1; i <= n; i++) {
            fibonacciNums.add(f2);
            int next = f1 + f2;
            f1 = f2;
            f2 = next;
        }
    }

    /**
     * Method for receiving list of Fibonacci numbers.
     * @return list of Fibonacci numbers
     */
    public List<Integer> getFibonacciNums() {
        return fibonacciNums;
    }

    /**
     * Method for calculating percentage of odd numbers.
     * @return percentage of odd numbers
     */
    public int calcOddNumsPercentage() {
        int totalNumsCount = getFibonacciNums().size();
        int oddNumsCount = findOddNums().size();

        return oddNumsCount * HUNDRED / totalNumsCount;
    }

    /**
     * Method for calculating percentage of even numbers.
     * @return percentage of even numbers
     */
    public int calcEvenNumsPercentage() {
        int totalNumsCount = getFibonacciNums().size();
        int evenNumsCount = findEvenNums().size();

        return evenNumsCount * HUNDRED / totalNumsCount;
    }

    /**
     * Method for finding even Fibonacci numbers.
     * @return list of even Fibonacci numbers
     */
    private List<Integer> findEvenNums() {
        List<Integer> evenNums = new ArrayList<>();
        for (Integer fibonacciNum : fibonacciNums) {
            if (fibonacciNum % 2 == 0) {
                evenNums.add(fibonacciNum);
            }
        }
        return evenNums;
    }

    /**
     * Method for finding odd Fibonacci numbers.
     * @return list of odd Fibonacci numbers
     */
    private List<Integer> findOddNums() {
        List<Integer> oddNums = new ArrayList<>();
        for (Integer fibonacciNum : fibonacciNums) {
            if (!(fibonacciNum % 2 == 0)) {
                oddNums.add(fibonacciNum);
            }
        }
        return oddNums;
    }
}
