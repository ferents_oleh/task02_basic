package number;

import java.util.ArrayList;
import java.util.List;

/**
 * The {@code number.NumberHandler} class for generating even and odd numbers.
 *
 * @author Oleh Ferents
 */
public class NumberHandler {
    /**
     * List for containing even numbers.
     */
    private List<Integer> evenNums = new ArrayList<>();

    /**
     * List for containg odd numbers.
     */
    private List<Integer> oddNums = new ArrayList<>();

    /**
     * Field that contains first number of interval.
     */
    private int start;

    /**
     * Field that contains last number of interval.
     */
    private int end;

    /**
     * Constructor for creating instance with interval edges.
     * @param firstNumber - first number
     * @param lastNumber - last number
     */
    public NumberHandler(final int firstNumber, final int lastNumber) {
       start = firstNumber;
       end = lastNumber;
    }

    /**
     * Method for receiving even numbers.
     * @return list of even numbers
     */
    public List<Integer> getEvenNums() {
        evenNums.clear();
        fillEvenNumsList();
        return evenNums;
    }

    /**
     * Method for receiing odd numbers.
     * @return list of odd numbers
     */
    public List<Integer> getOddNums() {
        oddNums.clear();
        fillOddNumsList();
        return oddNums;
    }

    /**
     * Method for filling list of even numbers.
     */
    private void fillEvenNumsList() {
        for (int i = start; i <= end; i++) {
            if (i % 2 == 0) {
                evenNums.add(i);
            }
        }
    }

    /**
     * Method for filling list of odd numbers.
     */
    private void fillOddNumsList() {
        for (int i = end; i >= start; i--) {
            if (!(i % 2 == 0)) {
                oddNums.add(i);
            }
        }
    }

    /**
     * Method for finding max value of odd numbers.
     * @return max value
     */
    public int getOddMaxNum() {
        return getOddNums()
                .stream()
                .mapToInt(Integer::intValue)
                .max()
                .getAsInt();
    }

    /**
     * Method for finding max value of even numbers.
     * @return max value
     */
    public int getEvenMaxNum() {
        return getEvenNums()
                .stream()
                .mapToInt(Integer::intValue)
                .max()
                .getAsInt();
    }
}
