package helpers;

import fibonacci.FibonacciHandler;
import number.NumberHandler;

import java.util.Scanner;

/**
 *The {@code helpers.ConsoleInputOutput} class contains methods
 * for console input and output.
 *
 * @author Oleh Ferents
 */


public class ConsoleInputOutput {

    /**
    * The {@code NumberHandler} class for getting even and odd numbers.
    */
    private NumberHandler numberHandler;

    /**
     * The {@code FibonacciHandler} class for generate N Fibonacci numbers.
     */
    private FibonacciHandler fibonacciHandler = new FibonacciHandler();

    /**
     * Method for receiving interval of numbers for {@link NumberHandler}.
     */
    public void enterInterval() {
        Scanner scanner = new Scanner(System.in);

        System.out.print("Enter start of interval - ");
        int start = scanner.nextInt();

        System.out.print("Enter end of interval - ");
        int end = scanner.nextInt();
        numberHandler = new NumberHandler(start, end);
    }

    /**
     * Method for printing even numbers
     * using {@link NumberHandler#getEvenNums()}.
     */
    public void printEvenNums() {
        System.out.println("Even numbers:");
        numberHandler.getEvenNums().forEach(System.out::println);
    }

    /**
     * Method for printing odd numbers using {@link NumberHandler#getOddNums()}.
     */
    public void printOddNums() {
        System.out.println("Odd numbers:");
        numberHandler.getOddNums().forEach(System.out::println);
    }

    /**
     * Method for printing sum of even numbers
     * using {@link NumberHandler#getEvenNums()}.
     */
    public void printEvenNumsSum() {
        int sum = numberHandler.getEvenNums()
                .stream()
                .mapToInt(Integer::intValue)
                .sum();
        System.out.println("Sum of even numbers is - " + sum);
    }

    /**
     * Method for printing sum of odd numbers using
     * {@link NumberHandler#getOddNums()}.
     */
    public void printOddNumsSum() {
        int sum = numberHandler.getOddNums()
                .stream()
                .mapToInt(Integer::intValue)
                .sum();
        System.out.println("Sum of odd numbers is - " + sum);
    }

    /**
     * Method receiving size of Fibonacci numbers list and
     * building list of Fibonacci numbers using
     * {@link NumberHandler#getOddNums()}
     * and {@link NumberHandler#getEvenNums()}.
     */
    public void enterFibonacciSetSize() {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Enter fibonacci n");

        int size = scanner.nextInt();

        int biggestOddNum = numberHandler.getOddMaxNum();

        int biggestEvenNum = numberHandler.getEvenMaxNum();

        fibonacciHandler.buildSet(size, biggestOddNum, biggestEvenNum);
    }

    /**
     * Method for printing list of Fibonacci numbers
     * using {@link FibonacciHandler#getFibonacciNums()}.
     */
    public void printFibonacciNumbers() {
        System.out.println("Fibonacci numbers:");
        fibonacciHandler.getFibonacciNums().forEach(System.out::println);
    }

    /**
     * Method for printing percentage of odd and even numbers
     * in Fibonacci numbers list.
     */
    public void printPercentage() {
        System.out.println("Percentage of even Fibonacci numbers = "
                + fibonacciHandler.calcEvenNumsPercentage() + "%");
        System.out.println("Percentage of odd Fibonacci numbers = "
                + fibonacciHandler.calcOddNumsPercentage() + "%");
    }
}
