import helpers.ConsoleInputOutput;

public class Application {
    public static void main(String[] args) {
        ConsoleInputOutput consoleInputOutput = new ConsoleInputOutput();

        consoleInputOutput.enterInterval();

        consoleInputOutput.printEvenNums();
        consoleInputOutput.printOddNums();

        consoleInputOutput.printEvenNumsSum();
        consoleInputOutput.printOddNumsSum();

        consoleInputOutput.enterFibonacciSetSize();
        consoleInputOutput.printFibonacciNumbers();

        consoleInputOutput.printPercentage();
    }
}
